﻿using Repositories.Interface;
using System;
using System.Data.Entity;

namespace Repositories
{
    /// <summary>
    /// "UnitOfWork" class to access entity and DB context.
    /// </summary> 
    public class UnitOfWork<TContext> : IUnitOfWork<TContext> where TContext : DbContext, new()
    {
        private TContext context { get; set; }

        public UnitOfWork()
        {
            this.context = new TContext();
        }

        public TContext GetContext()
        {
            return context;
        }

        ////Set context for type T entity
        //public IRepositoryBase<T> GenericRepository<T>() where T : class
        //{
        //    return new RepositoryBase<T>(context);
        //}

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
