﻿using System.Data.Entity.Infrastructure;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repositories
{
    /// <summary>
    /// "Repository" class to perform all CRUD operation.
    /// </summary>    
    public class RepositoryBase<TEntity, TContext> : IRepositoryBase<TEntity, TContext>
        where TEntity : class
        where TContext : DbContext
    {
        private DbContext context { get; set; }

        internal DbSet<TEntity> dbSet;

        public RepositoryBase(IUnitOfWork<TContext> unitOfWork)
        {
            this.context = (DbContext)unitOfWork.GetContext();
            this.context.Database.CommandTimeout = 6000;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> All()
        {
            return dbSet.AsQueryable();
        }
		public virtual IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> predicate)
        {
            return dbSet.Where(predicate).AsQueryable<TEntity>();
        }
        public virtual TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return All().FirstOrDefault(predicate);
        }
        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }
        public virtual void Insert(TEntity entity)
        {
            dbSet.Add(entity);
        }
        public virtual void Update(TEntity entityToUpdate)
        {
            dbSet.Attach(entityToUpdate);
            context.Entry(entityToUpdate).State = System.Data.Entity.EntityState.Modified;
        }

	}
}
