﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Repositories.Interface
{
    public interface IRepositoryBase<TEntity, TContext>
    {
        /// <summary>
        /// Gets all objects from database
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> All();
        /// <summary>
        /// Gets objects from database by filter.
        /// </summary>
        /// <param name="predicate">Specified a filter</param>
        /// <returns></returns>
        IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> predicate);
		/// <summary>
		/// Gets single objects from database by filter.
		/// </summary>
		/// <param name="predicate">Specified a filter</param>
		/// <returns></returns>
		TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        TEntity GetByID(object id);
        void Insert(TEntity entity);
        void Update(TEntity entityToUpdate);


    }
}
