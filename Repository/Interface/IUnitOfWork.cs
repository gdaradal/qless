﻿using System;
using System.Data.Entity;

namespace Repositories.Interface
{
    public interface IUnitOfWork<TContext> where TContext: DbContext, IDisposable
    {
        TContext GetContext();

        void Save();

        void Dispose(bool disposing);
    }
}
