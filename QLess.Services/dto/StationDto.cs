﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class StationDto
	{
		public int Id{ get; set; }
		public string StationName { get; set; }
	}
}
