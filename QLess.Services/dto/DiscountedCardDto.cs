﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class DiscountedCardDto
	{
		public string CardId { get; set; }
		public string IdType { get; set; }
		public string IdNumber { get; set; }
	}
}
