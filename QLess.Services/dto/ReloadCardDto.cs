﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class ReloadCardDto
	{
		public string CardId { get; set; }
		public double AmountToLoad { get; set; }
		public double CashValue { get; set; }
	}
}
