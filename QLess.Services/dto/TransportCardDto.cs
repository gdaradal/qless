﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class TransportCardDto
	{
		public int Id { get; set; }
		public string CardId { get; set; }
		public DateTime PurchaseDate { get; set; }
		public DateTime ExpiryDate { get; set; }
		public bool DiscountedCard { get; set; }
		public DateTime? DiscountRegDate { get; set; }
		public double Balance { get; set; }
	}
}
