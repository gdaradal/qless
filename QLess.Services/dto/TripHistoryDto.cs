﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class TripHistoryDto
	{
		public string CardId { get; set; }
		public DateTime TransactionDate { get; set; }
		public string StationFrom { get; set; }
		public string StationTo { get; set; }
		public Nullable<double> Charged { get; set; }
		public string Type { get; set; }
		public Nullable<double> DiscountApplied { get; set; }
	}
}
