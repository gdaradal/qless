﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public interface ITripService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		string enter(TripDto model);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		string exit(TripDto model);
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cardId"></param>
		/// <returns></returns>
		IEnumerable<TripHistoryDto> history(string cardId);
	}
}
