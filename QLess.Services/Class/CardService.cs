﻿using QLess.DataLayer;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class CardService : ICardService
	{
		private readonly IUnitOfWork<QLessEntities> unitOfWork;
		private readonly IRepositoryBase<TransportCard, QLessEntities> transportCardRepo;
		private readonly IRepositoryBase<TransportCardHistory, QLessEntities> transportCardHistoryRepo;

		public CardService(
				IUnitOfWork<QLessEntities> unitOfWork, 
				IRepositoryBase<TransportCard, QLessEntities> transportCardRepo, 
				IRepositoryBase<TransportCardHistory, QLessEntities> transportCardHistoryRepo
			)
		{
			this.unitOfWork = unitOfWork;
			this.transportCardRepo = transportCardRepo;
			this.transportCardHistoryRepo = transportCardHistoryRepo;
		}


		public bool Delete(int Id)
		{
			return true;
		}

		public bool Save(TransportCardDto model)
		{
			var result = false;
			try
			{
				if (model.Id == 0)
				{
					//insert
					var initialLoad = 100;
					var date = DateTime.Now;
					var entity = new TransportCard();
					var history = new TransportCardHistory();


					entity.CardId = model.CardId;
					entity.PurchaseDate = date;
					entity.ExpiryDate = date.AddYears(5);
					entity.DiscountedCard = false;
					entity.Balance = initialLoad;
					transportCardRepo.Insert(entity);

					history.Charged = initialLoad;
					history.Type = "L";
					history.TransportCardId = entity.Id;
					history.TransactionDate = DateTime.Now;
					history.StationIdFrom = 1;
					history.StationIdTo = 1;
					history.DiscountApplied = 0;

					transportCardHistoryRepo.Insert(history);

					this.unitOfWork.Save();
					result = true;
				}

				return result;

			}
			catch (Exception ex)
			{

				throw ex;
			}


		}


	}
}
