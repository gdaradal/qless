﻿using QLess.DataLayer;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class StationService : IStationService
	{
		private readonly IRepositoryBase<Station, QLessEntities> stationRepo;

		public StationService(
				IRepositoryBase<Station, QLessEntities> stationRepo
			)
		{
			this.stationRepo = stationRepo;
		}


		public IEnumerable<StationDto> List()
		{
			try
			{
				var data =
					(
					from s in stationRepo.All()
					select new StationDto
					{
						Id = s.Id,
						StationName = s.StationName
					}
					).ToList();
				return data;
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}
	}
}
