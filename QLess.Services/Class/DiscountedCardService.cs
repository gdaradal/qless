﻿using QLess.DataLayer;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class DiscountedCardService : IDiscountedCardService
	{
		private readonly IUnitOfWork<QLessEntities> unitOfWork;
		private readonly IRepositoryBase<TransportCard, QLessEntities> transportCardRepo;

		public DiscountedCardService(
				IUnitOfWork<QLessEntities> unitOfWork, 
				IRepositoryBase<TransportCard, QLessEntities> transportCardRepo
			)
		{
			this.unitOfWork = unitOfWork;
			this.transportCardRepo = transportCardRepo;
		}

		public string Save(DiscountedCardDto model)
		{
			var errorMsg = string.Empty;
			try
			{
				var card = transportCardRepo.FirstOrDefault(c => c.CardId == model.CardId);
				if (card == null)
					return "Card not found or invalid!";


				errorMsg = IsCardAlreadyRegistered(card);
				if (errorMsg != "")
					return errorMsg;

				errorMsg = IsDateValidForRegistration(card);
				if (errorMsg != "")
					return errorMsg;



				card.DiscountedCard = true;
				card.IdNumber = model.IdNumber;
				card.IdType = model.IdType;
				card.DiscountRegDate = DateTime.Now;

				transportCardRepo.Update(card);
				this.unitOfWork.Save();
				return string.Empty;
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}

		private string IsDateValidForRegistration(TransportCard card) {
			var purchaseDate = card.PurchaseDate;
			var datetimeNow = DateTime.Now;
			if(purchaseDate.AddMonths(6) <= datetimeNow)
				return "This card is more than 6 months and not anymore valid for registration";

			return string.Empty;
		}

		private string IsCardAlreadyRegistered(TransportCard card) {

			if (card.DiscountedCard)
				return "Card already registered!";

			return string.Empty;
		}

	}
}
