﻿using QLess.DataLayer;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class TripService : ITripService
	{
		private readonly IUnitOfWork<QLessEntities> unitOfWork;
		private readonly IRepositoryBase<TransportCard, QLessEntities> transportCardRepo;
		private readonly IRepositoryBase<TransportCardHistory, QLessEntities> transportCardHistoryRepo;

		public IRepositoryBase<FareMatrix, QLessEntities> FareMatrixRepo { get; }

		public TripService(
				IUnitOfWork<QLessEntities> unitOfWork, 
				IRepositoryBase<TransportCard, QLessEntities> transportCardRepo, 
				IRepositoryBase<TransportCardHistory, QLessEntities> transportCardHistoryRepo,
				IRepositoryBase<FareMatrix, QLessEntities> fareMatrixRepo

			)
		{
			this.unitOfWork = unitOfWork;
			this.transportCardRepo = transportCardRepo;
			this.transportCardHistoryRepo = transportCardHistoryRepo;
			FareMatrixRepo = fareMatrixRepo;
		}

		public string enter(TripDto model)
		{
			var errorMsg = string.Empty;
			try
			{
				var card = transportCardRepo.FirstOrDefault(c => c.CardId == model.CardId);

				if (card == null)
					return "Card not found or invalid!";

				if (card.ExpiryDate <= DateTime.Now)
					return "Card already expired";

				errorMsg = hasPendingRide(card.Id);
				if (errorMsg != "")
					return errorMsg;

				var history = new TransportCardHistory();

				history.Charged = 0;
				history.Type = "T";
				history.TransportCardId = card.Id;
				history.TransactionDate = DateTime.Now;
				history.StationIdFrom = model.StationId;
				history.DiscountApplied = 0;

				transportCardHistoryRepo.Insert(history);
				this.unitOfWork.Save();
				return errorMsg;
			}
			catch (Exception)
			{
				throw;
			}
		}

		public string exit(TripDto model)
		{
			var errorMsg = string.Empty;
			double fare = 0;
			double discounts = 0;
			DateTime datefrom = DateTime.Now.Date;
			DateTime dateto = DateTime.Now.AddDays(1).Date;
			try
			{
				var card = getCard(model.CardId);
				var history = transportCardHistoryRepo.Filter(c => c.TransportCardId == card.Id && c.Charged == 0 && c.Type == "T").ToList();
				var history2 =
					from h in transportCardHistoryRepo.Filter(c => c.Charged != 0 && c.Type == "T")
					where h.TransactionDate >= datefrom && h.TransactionDate < dateto
					select new
					{
						Id = h.Id
					};
					
				if(history.Count() == 0)
					return "Card does not have pending transaction. Enter the transit first!";


				if (history.Count() > 1)
					return "Card has multiple entry.  transaction invalid!";

				var transaction = history.FirstOrDefault();
				fare = getFare((int)transaction.StationIdFrom, model.StationId);

				if (card.DiscountedCard)
				{
					// less 20%
					fare -= (fare * .2);
					discounts += .2;
				}

				var todayTrips = history2.Count();
				if (todayTrips != 0 && todayTrips < 4) {



					// less %3 in fare
					fare -= (fare * .03);
					discounts += .03;
				}

				if (card.Balance < fare)
					return "Not enough balance.  Please reload!";


				transaction.Charged = fare;
				transaction.DiscountApplied = discounts;
				transaction.StationIdTo = model.StationId;

				transportCardHistoryRepo.Update(transaction);

				card.Balance -= fare;
				transportCardRepo.Update(card);

				this.unitOfWork.Save();


				return errorMsg;
			}
			catch (Exception ex)
			{

				throw ex;
			}
		}

		public IEnumerable<TripHistoryDto> history(string cardId)
		{
			try
			{
				var data =
					(
						from h in transportCardHistoryRepo.All()
						where h.TransportCard.CardId == cardId
						select new TripHistoryDto
						{
							CardId = h.TransportCard.CardId,
							Charged = h.Charged,
							DiscountApplied = h.DiscountApplied,
							StationFrom = h.Station.StationName,
							StationTo = h.Station1.StationName,
							TransactionDate = h.TransactionDate,
							Type = h.Type
						}
					).ToList();

				return data;

			}
			catch (Exception ex)
			{

				throw ex;
			}
		}

		private double getFare(int stationFrom, int stationTo) {
			double baseFare = 11;
			double fare = 0;
			var matrix = FareMatrixRepo.All().ToList();
			var origin = matrix.Where(s => s.Id == stationFrom).Select(c => c).ToArray();

			string field = "";
			foreach (PropertyInfo prop in origin[0].GetType().GetProperties())
			{
				var p = prop;
				if (p.Name.ToLower() != "id")
				{
					var amt = p.GetValue(origin[0]);
					if (double.Parse(amt.ToString()) == baseFare)
					{
						field = p.Name.ToLower();
						break;
					}
				}
			}

			var destination = matrix.Where(s => s.Id == stationTo).Select(c => c).ToArray();
			foreach (PropertyInfo prop in destination[0].GetType().GetProperties())
			{
				var p = prop;
				if (p.Name.ToLower() == field)
				{
					fare = double.Parse((p.GetValue(destination[0])).ToString());
					break;
				}
			}

			return fare;
		}
		private string hasPendingRide(int cardId) {
			var data = transportCardHistoryRepo.Filter(c => c.TransportCardId == cardId && c.Charged == 0 && c.Type == "T");

			if (data.Count() > 0)
				return "Has pending ride.  You need to exit first.";
		


			return string.Empty;
		}
		private TransportCard getCard(string cardId) {
			return transportCardRepo.FirstOrDefault(c => c.CardId == cardId);
		}
	}   
}
