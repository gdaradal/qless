﻿using QLess.DataLayer;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLess.Services
{
	public class ReloadCardService : IReloadCardService
	{
		private readonly IUnitOfWork<QLessEntities> unitOfWork;
		private readonly IRepositoryBase<TransportCardHistory, QLessEntities> transportCardHistoryRepo;
		private readonly IRepositoryBase<TransportCard, QLessEntities> transportCardRepo;

		public ReloadCardService(
				IUnitOfWork<QLessEntities> unitOfWork, 
				IRepositoryBase<TransportCardHistory, QLessEntities> transportCardHistoryRepo,
				IRepositoryBase<TransportCard, QLessEntities> transportCardRepo

			)
		{
			this.unitOfWork = unitOfWork;
			this.transportCardHistoryRepo = transportCardHistoryRepo;
			this.transportCardRepo = transportCardRepo;
		}

		public IRepositoryBase<TransportCard, QLessEntities> TransportCardRepo { get; }

		public string Save(ReloadCardDto model) {
			try
			{
				var card = transportCardRepo.FirstOrDefault(c => c.CardId == model.CardId);
				if (card == null)
					return "Card not found or invalid!";

				if (card.ExpiryDate <= DateTime.Now)
					return "Card already expired";

				card.ExpiryDate = DateTime.Now.AddYears(5);
				card.Balance += model.AmountToLoad;
				transportCardRepo.Update(card);

				var history = new TransportCardHistory();
				history.Charged = model.AmountToLoad;
				history.Type = "L";
				history.TransportCardId = card.Id;
				history.TransactionDate = DateTime.Now;
				history.StationIdFrom = 1;
				history.StationIdTo = 1;
				history.DiscountApplied = 0;
				transportCardHistoryRepo.Insert(history);

				this.unitOfWork.Save();
				return string.Empty;
			}
			catch (Exception ex)
			{
				throw ex;
			}


		}
	}
}
