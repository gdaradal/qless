//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QLess.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class TransportCardHistory
    {
        public int Id { get; set; }
        public int TransportCardId { get; set; }
        public System.DateTime TransactionDate { get; set; }
        public Nullable<int> StationIdFrom { get; set; }
        public Nullable<int> StationIdTo { get; set; }
        public Nullable<double> Charged { get; set; }
        public string Type { get; set; }
        public Nullable<double> DiscountApplied { get; set; }
    
        public virtual Station Station { get; set; }
        public virtual Station Station1 { get; set; }
        public virtual TransportCard TransportCard { get; set; }
    }
}
