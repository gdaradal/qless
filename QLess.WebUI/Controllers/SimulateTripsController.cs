﻿using QLess.Services;
using QLess.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLess.WebUI.Controllers
{
	[RoutePrefix("simulatetrips")]
    public class SimulateTripsController : Controller
    {
		private readonly IStationService stationService;
		private readonly ITripService tripService;

		public SimulateTripsController(
				IStationService stationService, 
				ITripService tripService
			)
		{
			this.stationService = stationService;
			this.tripService = tripService;
		}

        // GET: SimulateTrips
        public ActionResult Index()
        {
            return View();
        }

		[HttpGet]
		[Route("liststations")]
		public JsonResult ListStations() {
			try
			{
				var stations = stationService.List();
				return Json(new { stations }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{

				throw;
			}
		}

		[HttpPost]
		[Route("enter")]
		public JsonResult enter(TripsInsertModel model) {
			try
			{
				var data = new TripDto();

				data.CardId = model.CardId;
				data.StationId = model.StationId;

				var errorMsg = tripService.enter(data);
				return Json(new { success = errorMsg == "" ? true : false, errorMsg });
			}
			catch (Exception)
			{
				return Json(new { success=true, errorMsg="Transaction Failed.  Please try again!" });
			}

		}

		[HttpPost]
		[Route("exit")]
		public JsonResult exit(TripsInsertModel model)
		{
			try
			{
				var data = new TripDto();

				data.CardId = model.CardId;
				data.StationId = model.StationId;

				var errorMsg = tripService.exit(data);
				return Json(new { success = errorMsg == "" ? true : false, errorMsg });
			}
			catch (Exception)
			{
				return Json(new { success = true, errorMsg = "Transaction Failed.  Please try again!" });
			}

		}

		[HttpGet]
		[Route("listhistory")]
		public JsonResult ListHistory(string cardId)
		{
			try
			{
				var data = tripService.history(cardId);
				var data2 =
					from d in data
					select new TripHistoryListingModel
					{
						CardId = d.CardId,
						Charged = d.Charged,
						DiscountApplied = d.DiscountApplied,
						StationFrom = d.StationFrom,
						StationTo = d.StationTo,
						TransactionDate = string.Format("{0: MM/dd/yyyy hh:mm}", d.TransactionDate),
						Type = d.Type
					};

				return Json(new { success = true, history = data2}, JsonRequestBehavior.AllowGet);
			}
			catch (Exception)
			{
				return Json(new { success = true, errorMsg = "Transaction Failed.  Please try again!" });
			}

		}
	}
}