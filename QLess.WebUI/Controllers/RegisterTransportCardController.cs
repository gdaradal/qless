﻿using QLess.Services;
using QLess.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLess.WebUI.Controllers
{
	[RoutePrefix("registertransportcard")]
    public class RegisterTransportCardController : Controller
    {
		private readonly IDiscountedCardService discountedCardService;

		public RegisterTransportCardController(
				IDiscountedCardService discountedCardService
			)
		{
			this.discountedCardService = discountedCardService;
		}

        // GET: RegisterTransportCard
        public ActionResult Index()
        {
            return View();
        }

		public JsonResult Save(TransportCardRegisterInsertUpdateModel model) {

			try
			{
				if (!ModelState.IsValid)
				{
					return Json(new { success = false, errorMsg = "Data is invalid" });
				}

				var data = new DiscountedCardDto();
				data.CardId = model.CardId;
				data.IdNumber = model.IdNumber;
				data.IdType = model.IdType;

				var errorMsg = discountedCardService.Save(data);
				return Json(new { success = errorMsg == "" ? true : false, errorMsg });

			}
			catch (Exception ex)
			{
				return Json(new { success = false, errorMsg=ex.Message });
			}

		}
    }
}