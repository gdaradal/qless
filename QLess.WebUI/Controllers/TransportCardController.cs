﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLess.Services;
using QLess.WebUI.Models;

namespace QLess.WebUI.Controllers
{
	[RoutePrefix("transportcard")]
    public class TransportCardController : Controller
    {


		private readonly ICardService cardService;

		public TransportCardController(){}

		public TransportCardController(ICardService cardService)
		{
			this.cardService = cardService;
		}

		// GET: TransportCard
		public ActionResult Index()
		{
			return View();
		}


		[HttpPost]
		[Route("save")]
		public JsonResult Save(TransportCardViewModel model) {
			try
			{
				var data = new TransportCardDto();
				data.Id = model.Id;
				data.CardId = model.CardId;

				var success = this.cardService.Save(data);

				return Json(new { success });

			}
			catch (Exception ex)
			{

				throw ex;
			}

		}
	}
}