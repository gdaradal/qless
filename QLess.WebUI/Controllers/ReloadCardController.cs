﻿using QLess.Services;
using QLess.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QLess.WebUI.Controllers
{
	[RoutePrefix("reloadcard")]
    public class ReloadCardController : Controller
    {
		private readonly IReloadCardService reloadCardService;

		public ReloadCardController(
				IReloadCardService reloadCardService
			)
		{
			this.reloadCardService = reloadCardService;
		}

        // GET: ReloadCard
        public ActionResult Index()
        {
            return View();
        }

		[HttpPost]
		[Route("save")]
		public JsonResult Save(ReloadInsertUpdateModel model)
		{

			try
			{
				if (!ModelState.IsValid)
				{
					return Json(new { success = false, errorMsg = "Data is invalid" });
				}

				var data = new ReloadCardDto();
				data.CardId = model.CardId;
				data.AmountToLoad = model.AmountToLoad;
				data.CashValue = model.CashValue;

				var errorMsg = reloadCardService.Save(data);
				return Json(new { success = errorMsg == "" ? true : false, errorMsg });

			}
			catch (Exception ex)
			{
				// log error to server
				return Json(new { success = false, errorMsg = ex.Message });
			}

		}
	}
}