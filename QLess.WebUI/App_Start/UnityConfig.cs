using QLess.Services;
using Repositories;
using Repositories.Interface;
using System.Web.Mvc;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;

namespace QLess.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

			container.RegisterType(typeof(IUnitOfWork<>), typeof(UnitOfWork<>), new PerResolveLifetimeManager());
			container.RegisterType(typeof(IRepositoryBase<,>), typeof(RepositoryBase<,>), new PerResolveLifetimeManager());

			// register all your components with the container here
			// it is NOT necessary to register your controllers

			container.RegisterType<ICardService, CardService>();
			container.RegisterType<IDiscountedCardService, DiscountedCardService>();
			container.RegisterType<IReloadCardService, ReloadCardService>();
			container.RegisterType<IStationService, StationService>();
			container.RegisterType<ITripService, TripService>();
			DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}