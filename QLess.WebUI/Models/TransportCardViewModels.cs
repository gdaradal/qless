﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QLess.WebUI.Models
{
	public class TransportCardViewModel
	{
		public int Id { get; set; }
		[Required]
		public string CardId { get; set; }
		public DateTime PurchaseDate { get; set; }
		public DateTime ExpiryDate { get; set; }
		public bool DiscountedCard { get; set; }
		public DateTime? DiscountRegDate { get; set; }
		public double Balance { get; set; }
	}
}