﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QLess.WebUI.Models
{
	public class ReloadInsertUpdateModel
	{
		[Required]
		public string CardId { get; set; }
		[Required]
		public double AmountToLoad { get; set; }
		[Required]
		public double CashValue { get; set; }

	}
}