﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QLess.WebUI.Models
{
	public class TransportCardRegisterInsertUpdateModel
	{
		[Required]
		public string CardId { get; set; }
		[Required]
		public string IdType { get; set; }
		[Required]
		public string IdNumber { get; set; }
	}
}