﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLess.WebUI.Models
{
	public class TripsInsertModel
	{
		public string CardId { get; set; }
		public int StationId { get; set; }
	}
}