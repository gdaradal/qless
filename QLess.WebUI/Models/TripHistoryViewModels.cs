﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLess.WebUI.Models
{
	public class TripHistoryListingModel
	{
		public string CardId { get; set; }
		public string TransactionDate { get; set; }
		public string StationFrom { get; set; }
		public string StationTo { get; set; }
		public Nullable<double> Charged { get; set; }
		public string Type { get; set; }
		public Nullable<double> DiscountApplied { get; set; }
	}
}